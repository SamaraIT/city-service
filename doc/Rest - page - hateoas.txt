# Spring Data Rest

{
  "name": "Zagreb",
  "description": "Croatia",
  "population": 321321321,
  "created": "2020-05-21T15:06:41.3372731",
  "_links": {
    "self": {
      "href": "http://localhost:9393/cities-play/cities/4"
    },
    "city": {
      "href": "http://localhost:9393/cities-play/cities/4"
    }
  }
}





{
  "_embedded": {
    "cities": [
      {
        "name": "TOTO",
        "description": "Croatia",
        "population": 321321321,
        "created": "2020-05-21T14:55:31",
        "_links": {
          "self": {
            "href": "http://localhost:9393/cities-play/cities/1"
          },
          "city": {
            "href": "http://localhost:9393/cities-play/cities/1"
          }
        }
      },
      {
        "name": "Zagreb",
        "description": "Croatia",
        "population": 321321321,
        "created": "2020-05-21T15:06:41",
        "_links": {
          "self": {
            "href": "http://localhost:9393/cities-play/cities/4"
          },
          "city": {
            "href": "http://localhost:9393/cities-play/cities/4"
          }
        }
      },
      {
        "name": "Osijek",
        "description": "Croatia",
        "population": 22,
        "created": "2020-05-21T15:24:42",
        "_links": {
          "self": {
            "href": "http://localhost:9393/cities-play/cities/5"
          },
          "city": {
            "href": "http://localhost:9393/cities-play/cities/5"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9393/cities-play/cities{?page,size,sort}",
      "templated": true
    },
    "profile": {
      "href": "http://localhost:9393/cities-play/profile/cities"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 3,
    "totalPages": 1,
    "number": 0
  }
}

## Page vraca rest controller
{
    "content": [
        {
            "id": 5,
            "name": "Osijek",
            "description": "Croatia",
            "population": 22,
            "created": "2020-05-21T15:24:42"
        },
        {
            "id": 4,
            "name": "Zagreb",
            "description": "Croatia",
            "population": 321321321,
            "created": "2020-05-21T15:06:41"
        },
        {
            "id": 1,
            "name": "TOTO",
            "description": "Croatia",
            "population": 321321321,
            "created": "2020-05-21T14:55:31"
        }
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "offset": 0,
        "pageNumber": 0,
        "pageSize": 20,
        "unpaged": false,
        "paged": true
    },
    "last": true,
    "totalPages": 1,
    "totalElements": 3,
    "number": 0,
    "size": 20,
    "sort": {
        "sorted": true,
        "unsorted": false,
        "empty": false
    },
    "numberOfElements": 3,
    "first": true,
    "empty": false
}