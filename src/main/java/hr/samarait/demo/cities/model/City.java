package hr.samarait.demo.cities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.samarait.demo.auth.model.UserInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@ToString(exclude = "users")
@EqualsAndHashCode(exclude = "users")
@Entity
public class City {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String name;

  @NotBlank
  private String description;

  @NotNull
  private Integer population;

  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
    name = "user_city",
    joinColumns = @JoinColumn(name = "city_id"),
    inverseJoinColumns = @JoinColumn(name = "user_id"))
  private Set<UserInfo> users = new HashSet<>();

  private LocalDateTime created;

  public City(String name, String description, Integer population) {
    this.name = name;
    this.description = description;
    this.population = population;
    this.created = LocalDateTime.now();
  }
}
