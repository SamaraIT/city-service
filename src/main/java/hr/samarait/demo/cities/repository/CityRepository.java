package hr.samarait.demo.cities.repository;

import hr.samarait.demo.cities.model.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CityRepository extends Repository<City, Integer> {

  City findById(Integer id);

  City save(City request);

  List<City> findAll(Pageable pageable);

  @Query(value = "select city.* from city join user_city uc on city.id = uc.city_id" +
    "                       group by city_id" +
    "                       order by count(*) desc" +
    "                       limit :size", nativeQuery = true)
  List<City> findAllByFavourites(@Param("size") int size);
}
