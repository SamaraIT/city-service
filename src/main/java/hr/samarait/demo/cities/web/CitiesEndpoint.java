package hr.samarait.demo.cities.web;

import hr.samarait.demo.auth.model.UserInfo;
import hr.samarait.demo.auth.service.UserInfoService;
import hr.samarait.demo.cities.model.City;
import hr.samarait.demo.cities.repository.CityRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/cities")
@Tag(name = "Cities", description = "API for creating a city, fetching cities and adding/removing city to/from favorites ")
@Slf4j
public class CitiesEndpoint {

  private static final String FAVORITES = "/favorites";

  private final CityRepository cityRepository;
  private final UserInfoService userInfoService;

  @Value("${spring.data.web.pageable.default-page-size:20}")
  private int defaultPageSize = 20;

  public CitiesEndpoint(CityRepository cityRepository, UserInfoService userInfoService) {
    this.cityRepository = cityRepository;
    this.userInfoService = userInfoService;
  }

  @GetMapping
  @PreAuthorize("permitAll()")
  @Operation(summary = "Retrieve cities",
    parameters = {
      @Parameter(name = "page", in = ParameterIn.QUERY, schema = @Schema(type = "integer"),
        description = "Results page you want to retrieve (0..N)"),
      @Parameter(name = "size", in = ParameterIn.QUERY, schema = @Schema(type = "integer"),
        description = "Number of records per page."),
      @Parameter(name = "sort", in = ParameterIn.QUERY, schema = @Schema(type = "string"),
        description = "Sorting criteria in the format: property(,asc|desc). " +
          "Default sort order is ascending. " +
          "Multiple sort criteria are supported.")
    })
  public List<City> fetchCities(@Parameter(hidden = true) @SortDefault(sort = "created", direction = Sort.Direction.DESC) Pageable pageable) {
    log.info("fetchCities START, pageable={}", pageable);

    List<City> cities = cityRepository.findAll(pageable);

    log.info("fetchCities END {}", cities);
    return cities;
  }

  @GetMapping(FAVORITES)
  @PreAuthorize("permitAll()")
  @Operation(summary = "Retrieve most favorited cities")
  public List<City> fetchFavCities(@RequestParam(required = false) Integer size) {
    if (Objects.isNull(size))
      size = defaultPageSize;
    log.info("fetchFavCities START size={}", size);
    List<City> cities = cityRepository.findAllByFavourites(size);
    log.info("fetchFavCities END {}", cities);
    return cities;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("isAuthenticated()")
  @Operation(summary = "Create a city")
  public City createCity(@RequestBody City request) {
    log.info("createCity START {}", request);
    City saved = cityRepository.save(request);
    log.info("createCity END {}", saved);
    return saved;
  }

  @PutMapping(FAVORITES + "/{cityId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("isAuthenticated()")
  @Operation(summary = "Add a city to favorites")
  public void addToFav(@PathVariable("cityId") Integer cityId, Principal principal) {
    log.info("addToFav START user={}, cityId={}", principal.getName(), cityId);
    City city = cityRepository.findById(cityId);
    UserInfo userInfo = userInfoService.findByUsername(principal.getName());
    city.getUsers().add(userInfo);
    cityRepository.save(city);
    log.info("addToFav END {}", principal);
  }

  @DeleteMapping(FAVORITES + "/{cityId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("isAuthenticated()")
  @Operation(summary = "Remove a city from favorites")
  public void delFromFav(@PathVariable("cityId") Integer cityId, Principal principal) {
    log.info("delFromFav START user={}, cityId={}", principal.getName(), cityId);
    City city = cityRepository.findById(cityId);
    UserInfo userInfo = userInfoService.findByUsername(principal.getName());
    boolean removed = city.getUsers().remove(userInfo);
    log.debug("isCity removed? ={}", removed);
    if (removed)
      cityRepository.save(city);
    log.info("delFromFav END {}", principal);
  }

}
