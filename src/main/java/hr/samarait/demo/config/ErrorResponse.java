package hr.samarait.demo.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorResponse {
  public static final String ISO_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

  @JsonFormat(pattern = ISO_DATETIME_FORMAT)
  private final LocalDateTime timestamp = LocalDateTime.now();

  private final List<String> errors;

  public ErrorResponse(String text) {
    errors = new ArrayList<>();
    errors.add(text);
  }

  public ErrorResponse(List<String> errors) {
    this.errors = errors;
  }
}
