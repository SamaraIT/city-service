package hr.samarait.demo.config;

import hr.samarait.demo.auth.web.JwtAuthenticationEntryPoint;
import hr.samarait.demo.auth.web.JwtRequestFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final JwtAuthenticationEntryPoint authenticationEntryPoint;
  private final JwtRequestFilter requestFilter;

  public WebSecurityConfig(JwtAuthenticationEntryPoint authenticationEntryPoint, JwtRequestFilter requestFilter) {
    this.authenticationEntryPoint = authenticationEntryPoint;
    this.requestFilter = requestFilter;
  }

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
      .csrf().disable()
      .authorizeRequests()
      .antMatchers("/auth", "/register", "/v3/api-docs/**", "/diagnostics/**").permitAll()
      //.anyRequest().authenticated() // using method security
      .and()
      .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    httpSecurity.addFilterBefore(requestFilter, UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

}
