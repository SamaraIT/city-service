package hr.samarait.demo.auth.repository;

import hr.samarait.demo.auth.model.UserInfo;
import org.springframework.data.repository.Repository;

public interface UserInfoRepository extends Repository<UserInfo, Integer> {

  boolean existsByUsername(String username);

  UserInfo findByUsername(String username);

  UserInfo save(UserInfo entity);

}
