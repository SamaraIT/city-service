package hr.samarait.demo.auth.service;

import hr.samarait.demo.auth.model.UserInfo;
import hr.samarait.demo.auth.repository.UserInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import java.util.ArrayList;

@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {

  private final UserInfoRepository userInfoRepository;

  public JwtUserDetailsService(UserInfoRepository userInfoRepository) {
    this.userInfoRepository = userInfoRepository;
  }

  @Override
  public UserDetails loadUserByUsername(@Email String username) throws UsernameNotFoundException {
    log.debug("loadUserByUsername START {}", username);
    UserInfo user = userInfoRepository.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
    log.debug("loadUserByUsername END {}", user);
    return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
  }

}
