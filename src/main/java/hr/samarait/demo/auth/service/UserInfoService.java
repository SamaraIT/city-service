package hr.samarait.demo.auth.service;

import hr.samarait.demo.auth.model.UserInfo;
import hr.samarait.demo.auth.model.UserResponse;
import hr.samarait.demo.auth.repository.UserInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Date;

@Service
@Slf4j
public class UserInfoService {

  private final UserInfoRepository userInfoRepository;
  private final PasswordEncoder passwordEncoder;

  public UserInfoService(UserInfoRepository userInfoRepository, PasswordEncoder passwordEncoder) {
    this.userInfoRepository = userInfoRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public UserResponse save(@Valid UserInfo user) {
    log.debug("save={}", user);
    if (userInfoRepository.existsByUsername(user.getUsername())) {
      throw new IllegalArgumentException("username already exist");
    }
    user.setCreated(new Date());
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    UserInfo saved = userInfoRepository.save(user);
    log.debug("saved={}", saved);

    UserResponse response = new UserResponse();
    response.setCreated(saved.getCreated());
    response.setUsername(saved.getUsername());
    return response;
  }

  public UserInfo findByUsername(String username) {
    return userInfoRepository.findByUsername(username);
  }
}
