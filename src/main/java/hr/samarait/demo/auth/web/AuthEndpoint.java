package hr.samarait.demo.auth.web;

import hr.samarait.demo.auth.model.JwtRequest;
import hr.samarait.demo.auth.model.JwtResponse;
import hr.samarait.demo.auth.model.UserInfo;
import hr.samarait.demo.auth.model.UserResponse;
import hr.samarait.demo.auth.service.JwtTokenService;
import hr.samarait.demo.auth.service.JwtUserDetailsService;
import hr.samarait.demo.auth.service.UserInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@Tag(name = "Authentication", description = "API for authentication and user registration")
@Slf4j
public class AuthEndpoint {

  private final AuthenticationManager authenticationManager;
  private final JwtTokenService jwtTokenService;
  private final JwtUserDetailsService userDetailsService;
  private final UserInfoService userInfoService;

  public AuthEndpoint(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService, JwtUserDetailsService userDetailsService, UserInfoService userInfoService) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenService = jwtTokenService;
    this.userDetailsService = userDetailsService;
    this.userInfoService = userInfoService;
  }

  @PostMapping("/register")
  @ResponseStatus(HttpStatus.CREATED)
  @Operation(summary = "Create a new user", description = "Create a new user with username and password")
  public UserResponse createUser(@RequestBody UserInfo user) {
    return userInfoService.save(user);
  }

  @Operation(summary = "Authenticate", description = "Authenticate user")
  @PostMapping("/auth")
  public JwtResponse createAuthenticationToken(@RequestBody JwtRequest request) {
    log.info("createAuthenticationToken START {}", request);
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
    final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
    final String token = jwtTokenService.generateToken(userDetails);
    JwtResponse jwtResponse = new JwtResponse(token);
    log.info("createAuthenticationToken END {}", jwtResponse);
    return jwtResponse;
  }

}
