package hr.samarait.demo.auth.web;

import hr.samarait.demo.auth.service.JwtTokenService;
import hr.samarait.demo.auth.service.JwtUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * populate SecurityContextHolder
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  private final JwtUserDetailsService jwtUserDetailsService;
  private final JwtTokenService jwtTokenService;

  public JwtRequestFilter(JwtUserDetailsService jwtUserDetailsService, JwtTokenService jwtTokenService) {
    this.jwtUserDetailsService = jwtUserDetailsService;
    this.jwtTokenService = jwtTokenService;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
    final String requestTokenHeader = request.getHeader("Authorization");
    String username = null;
    String jwtToken = null;

    if (Objects.nonNull(requestTokenHeader) && requestTokenHeader.startsWith("Bearer ")) {
      jwtToken = requestTokenHeader.substring(7);

      try {
        username = jwtTokenService.getUsernameFromToken(jwtToken);
      } catch (IllegalArgumentException e) {
        logger.info("Unable to get JWT Token");
      } catch (ExpiredJwtException e) {
        logger.info("JWT Token has expired");
      }
    } else if (requestTokenHeader == null) {
      logger.trace("Does not provide Authorization Header");
    } else if (!requestTokenHeader.startsWith("Bearer ")) {
      logger.warn("JWT Token does not begin with Bearer");
    }

    if (Objects.nonNull(username) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
      UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
      if (jwtTokenService.validateToken(jwtToken, userDetails)) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        logger.debug("set SecurityContextHolder " + authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
      }
    }

    chain.doFilter(request, response);
  }
}
