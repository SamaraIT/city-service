package hr.samarait.demo.auth.model;

import lombok.Data;

import java.util.Date;

@Data
public class UserResponse {
  private String username;
  private Date created;
}
