package hr.samarait.demo.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.samarait.demo.cities.model.City;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString(exclude = "favouriteCities")
@EqualsAndHashCode(exclude = "favouriteCities")
@NoArgsConstructor
@Entity
public class UserInfo implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private Integer id;

  @Email
  @Column(unique = true)
  private String username;
  @NotBlank
  private String password;

  @ManyToMany(mappedBy = "users", fetch = FetchType.LAZY)
  @JsonIgnore
  private Set<City> favouriteCities = new HashSet<>();

  private Date created;

  public UserInfo(String username, String password) {
    this.username = username;
    this.password = password;
  }
}
