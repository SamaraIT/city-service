package hr.samarait.demo;

import hr.samarait.demo.auth.model.UserInfo;
import hr.samarait.demo.auth.service.UserInfoService;
import hr.samarait.demo.cities.model.City;
import hr.samarait.demo.cities.repository.CityRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class CityServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(CityServiceApplication.class, args);
  }


 // @Bean
  public CommandLineRunner demo(UserInfoService userInfoService,
                                       CityRepository cityRepository) {
    return args -> {

      UserInfo user1 = new UserInfo("igor@samarait.hr", "igor");
      UserInfo user2 = new UserInfo("vedran@samarait.hr", "vedran");
      UserInfo user3 = new UserInfo("kristina@samarait.hr", "kristina");

      userInfoService.save(user1);
      userInfoService.save(user2);
      userInfoService.save(user3);

      City city1 = new City("Zagreb", "Bijeli grad. Zagreb kao glavni grad Hrvatske je jedini grad u Hrvatskoj koji ima položaj grada u županiji.", 790_017);
      City city2 = new City("Split", "Dalmacija", 178_192);
      City city3 = new City("Rijeka", "Kvarner", 128_384);
      City city4 = new City("Osijek", "Slavonija", 108_048);

      cityRepository.save(city1);
      cityRepository.save(city2);
      cityRepository.save(city3);
      cityRepository.save(city4);

      city1.getUsers().add(user1);
      city1.getUsers().add(user2);
      city1.getUsers().add(user3);
      cityRepository.save(city1);

      city2.getUsers().add(user1);
      cityRepository.save(city2);
      city3.getUsers().add(user1);
      cityRepository.save(city3);
      city4.getUsers().add(user1);
      city4.getUsers().add(user2);
      cityRepository.save(city4);
    };
  }

}
