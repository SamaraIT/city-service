package hr.samarait.demo.auth.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

/**
 * https://github.com/jwtk/jjwt
 */
@ExtendWith(MockitoExtension.class)
@Slf4j
class JwtTokenServiceTest {

  private final JwtTokenService tokenService = new JwtTokenService(5, "9FrawxpttjgVjnNtnyPrRGTa8n5daXWiwN5HssL3e2I");

  @Test
  void createAndVerifyToken() {
    final String user = "Samara";
    UserDetails userDetails = Mockito.mock(UserDetails.class);
    Mockito.when(userDetails.getUsername()).thenReturn(user);

    String token = tokenService.generateToken(userDetails);
    Assertions.assertNotNull(token);
    log.debug("token={}", token);
    String usernameFromToken = tokenService.getUsernameFromToken(token);
    log.debug("usernameFromToken={}", usernameFromToken);
    Assertions.assertEquals(user, usernameFromToken);
    Date expirationDateFromToken = tokenService.getExpirationDateFromToken(token);
    log.debug("expirationDateFromToken={}", expirationDateFromToken);
    Assertions.assertNotNull(expirationDateFromToken);
    boolean validateToken = tokenService.validateToken(token, userDetails);
    Assertions.assertTrue(validateToken);
  }

}
