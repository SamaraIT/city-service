package hr.samarait.demo.auth.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.samarait.demo.auth.model.JwtRequest;
import hr.samarait.demo.auth.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(AuthEndpoint.class)
//@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AuthEndpointTest {

  private static final String REGISTER = WebMvcLinkBuilder.linkTo(methodOn(AuthEndpoint.class).createUser(null)).withSelfRel().getHref();
  private static final String AUTH = WebMvcLinkBuilder.linkTo(methodOn(AuthEndpoint.class).createAuthenticationToken(null)).withSelfRel().getHref();

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void createUser() throws Exception {
    UserInfo request = new UserInfo("bosko@buha.hr", "bosko");
    mockMvc.perform(post(REGISTER)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.username").value(request.getUsername()))
      .andExpect(jsonPath("$.created").exists())
    ;
  }

  @Test
  void createUser_invalidUsername_ConstraintViolationException() throws Exception {
    UserInfo request = new UserInfo("bosko", "bosko");
    mockMvc.perform(post(REGISTER)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest()) // IllegalArgumentException
      .andExpect(jsonPath("$.errors[0]").value("username: must be a well-formed email address"))
      .andExpect(jsonPath("$.timestamp").exists())
    ;
  }

  @Test
  void createUser_alreadyExists_IllegalArgumentException() throws Exception {
    UserInfo request = new UserInfo("igor@samarait.hr", "bosko");
    mockMvc.perform(post(REGISTER)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.errors[0]").value("username already exist"))
      .andExpect(jsonPath("$.timestamp").exists())
    ;
  }

  @Test
  void createAuthenticationToken() throws Exception {
    JwtRequest request = new JwtRequest("igor@samarait.hr", "igor");
    mockMvc.perform(post(AUTH)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.token").exists())
    ;
  }

  @Test
  void createAuthenticationToken_wrongPass_BadCredentialsException() throws Exception {
    JwtRequest request = new JwtRequest("igor@samarait.hr", "");
    mockMvc.perform(post(AUTH)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.errors[0]").value("Bad credentials"))
      .andExpect(jsonPath("$.timestamp").exists())
    ;
  }

  @Test
  void createAuthenticationToken_wrongUsername_BadCredentialsException() throws Exception {
    JwtRequest request = new JwtRequest("nouser@samarait.hr", "igor");
    mockMvc.perform(post(AUTH)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.errors[0]").value("Bad credentials"))
      .andExpect(jsonPath("$.timestamp").exists())
    ;
  }
}
