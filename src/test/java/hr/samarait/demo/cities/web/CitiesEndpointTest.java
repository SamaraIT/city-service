package hr.samarait.demo.cities.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.samarait.demo.cities.model.City;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@WebMvcTest(CitiesEndpoint.class)
//@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CitiesEndpointTest {

  private static final String CITIES = WebMvcLinkBuilder.linkTo(CitiesEndpoint.class).withSelfRel().getHref();
  private static final LinkBuilder CITIES_FAV = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CitiesEndpoint.class).fetchFavCities(1008));
  private static final String USER = "igor@samarait.hr";

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;

  @Test
  @Order(1)
  void fetchCities_anonymous() throws Exception {
    mockMvc.perform(get(CITIES))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(content().string(containsString("population")))
      .andExpect(jsonPath("$.length()", is(10)))
    ;
  }

  @Test
  @Order(2)
  void fetchCities_with_size() throws Exception {
    mockMvc.perform(get(CITIES)
      .param("size", "2"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(content().string(containsString("population")))
      .andExpect(jsonPath("$.length()", is(2)))
    ;
  }

  @Test
  @Order(3)
  @WithUserDetails(USER)
  void fetchCities_authenticated() throws Exception {
    mockMvc.perform(get(CITIES))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(content().string(containsString("population")))
      .andExpect(jsonPath("$.length()", is(10)))
    ;
  }

  @Test
  @Order(10)
  void fetchFavoriteCities() throws Exception {
    mockMvc.perform(get(CITIES_FAV.withSelfRel().getHref()))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(content().string(containsString("population")))
      .andExpect(jsonPath("$.length()", is(4)))
    ;
  }

  @Test
  @Order(20)
  void createCity_anonymous() throws Exception {
    City request = new City("ZG", "desc", 12);
    mockMvc.perform(post(CITIES)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isUnauthorized())
      .andExpect(jsonPath("$.errors[0]").value("Access is denied"))
      .andExpect(jsonPath("$.timestamp").exists())
    ;
  }

  @Test
  @Order(21)
  @WithUserDetails(USER)
  void createCity_authenticated() throws Exception {
    City request = new City("Ljubljana", "Slovenia", 292_988);
    mockMvc.perform(post(CITIES)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.name").value(request.getName()))
      .andExpect(jsonPath("$.description").value(request.getDescription()))
      .andExpect(jsonPath("$.population").value(request.getPopulation()))
      .andExpect(jsonPath("$.created").exists())
    ;
  }

  @Test
  @Order(100) // has to be run after fetching favorite cities - number of cities will not be the same
  @WithUserDetails(USER)
  void addToFav() throws Exception {
    String cityId = "2";
    mockMvc.perform(put(CITIES_FAV.slash(cityId).withSelfRel().getHref()))
      .andDo(print())
      .andExpect(status().isNoContent())
    ;
  }

  @Test
  @Order(101)
  @WithUserDetails(USER)
  void delFromFav() throws Exception {
    String cityId = "2";
    mockMvc.perform(delete(CITIES_FAV.slash(cityId).withSelfRel().getHref()))
      .andDo(print())
      .andExpect(status().isNoContent())
    ;
  }

}
