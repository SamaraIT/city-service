DROP TABLE IF EXISTS user_city;
DROP TABLE IF EXISTS user_info;
DROP TABLE IF EXISTS city;

CREATE TABLE user_info(
                          id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                          username VARCHAR(50) NOT NULL UNIQUE,
                          password VARCHAR(500) NOT NULL,
                          created TIMESTAMP
);

CREATE TABLE city(
                     id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                     name VARCHAR(50) NOT NULL UNIQUE,
                     description VARCHAR(500) NOT NULL,
                     population INT(11),
                     created TIMESTAMP
);

CREATE TABLE user_city (
                           user_id INT UNSIGNED NOT NULL,
                           city_id INT UNSIGNED NOT NULL,
                           PRIMARY KEY (user_id, city_id),
                           CONSTRAINT user_city_user_FK
                               FOREIGN KEY (user_id) REFERENCES user_info(id),
                           CONSTRAINT user_city_city_FK
                               FOREIGN KEY (city_id) REFERENCES city(id)
);
