USE cities;

CREATE TABLE user_info(
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(500) NOT NULL,
  created TIMESTAMP
);

CREATE TABLE city(
  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL UNIQUE,
  description VARCHAR(500) NOT NULL,
  population INT(11),
  created TIMESTAMP
);

CREATE TABLE user_city (
    user_id INT UNSIGNED NOT NULL,
    city_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (user_id, city_id),
    CONSTRAINT user_city_user_FK
        FOREIGN KEY (user_id) REFERENCES user_info(id),
    CONSTRAINT user_city_city_FK
        FOREIGN KEY (city_id) REFERENCES city(id)
);


INSERT INTO city (id, name, description, population, created) VALUES (1, 'Zagreb', 'Bijeli grad. Zagreb kao glavni grad Hrvatske ...', 790017, '2020-05-01 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (2, 'Split', 'Dalmacija', 178192, '2020-05-02 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (3, 'Rijeka', 'Kvarner', 128384, '2020-05-03 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (4, 'Osijek', 'Slavonija', 108048, '2020-05-03 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (5, 'Zadar', 'Dalmacija ...', 71471, '2020-05-05 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (6, 'Slavonski Brod', 'Slavonija', 53.31, '2020-05-06 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (7, 'Nova Gradiška', 'Slavonija', 11821, '2020-05-07 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (8, 'Slatina', 'Slavonija', 10208, '2020-05-08 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (9, 'Trogir', '...', 10923, '2020-05-09 21:10:17');
INSERT INTO city (id, name, description, population, created) VALUES (10, 'Poreč', 'Dalmacija', 16696, '2020-05-10 21:10:17');

INSERT INTO user_info (id, username, password, created) VALUES (1, 'igor@samarait.hr', '$2a$10$uIqxx51M/.GfSmC3RE7v7eD8nWORQWA/YrsFyMni1DiW4P/ZQg.2u', '2020-05-21 21:10:17');
INSERT INTO user_info (id, username, password, created) VALUES (2, 'vedran@samarait.hr', '$2a$10$8A3j/NKCQRt7kQrpGklY0ey3bU6hUkq6bm8PPtbh4zoPNuPFOjB9a', '2020-05-21 21:10:17');
INSERT INTO user_info (id, username, password, created) VALUES (3, 'kristina@samarait.hr', '$2a$10$/tuohJVWPz2Q2q5dHVJ6OePJPMmW/9DIjnLE9yGLQo4DKV5f7RmmW', '2020-05-21 21:10:17');

INSERT INTO user_city (user_id, city_id) VALUES (1, 1);
INSERT INTO user_city (user_id, city_id) VALUES (1, 2);
INSERT INTO user_city (user_id, city_id) VALUES (1, 3);
INSERT INTO user_city (user_id, city_id) VALUES (1, 4);
INSERT INTO user_city (user_id, city_id) VALUES (2, 1);
INSERT INTO user_city (user_id, city_id) VALUES (2, 4);
INSERT INTO user_city (user_id, city_id) VALUES (3, 1);
