https://hub.docker.com/_/mariadb

## Create docker image to create tables
$ docker build -t cities-maria .

## Start docker
$ docker run --name cities-mariadb -p 3306:3306 -d cities-maria

## JDBC URL
- jdbc:mariadb://localhost:3306/cities
- jdbc:mariadb://192.168.99.100:3306/cities

## Logs
$ docker logs cities-mariadb

## Bash
$ docker exec -it cities-mariadb bash

### Connecting to MariaDB
mysql -u root -ppwd

```
CREATE DATABASE cities;
USE cities;
```

## Inspect
$ docker inspect cities-mariadb

## Remove
docker rm -f cities-mariadb

