## web service

### demo of
- JWT (see `JwtRequestFilter` and `JwtTokenService`)
- Method level security (see `CitiesEndpoint`)
- Many to many mapping (see `City`)
- swagger and openapi (see `CitiesEndpoint`)
- integration tests

### endpoints
#### auth
- create user
- login (generate token)
#### city
##### authenticated users can
- create new city (city has name, description and population)
- add city to favorites
- remove city from favorites
##### all users can
- fetch city (sort by)
- fetch city that are most liked

## Tech stack
 - Spring Boot 2.2 (web, security, JPA)
 - jjwt
 - springdoc-openapi-ui (documentation)
 - Docker (MariaDB)
 - Lombok
 - jacoco (code coverage)

### Documentation using Open API
- localhost:9393/cities-play/v3/api-docs/
- localhost:9393/cities-play/swagger-ui.html 

### Code coverage using jacoco
```
  $ mvn clean test
```  
Open in browser:    
file:///PROJECT_DIR/target/site/jacoco//index.html 

### Starting MariaDb using docker
1. build docker image
```
  $ cd db  
  $ docker build -t cities-maria .
```  
2. run docker image
```
  $ docker run --name cities-mariadb -p 3306:3306 -d cities-maria
```  

# Ref
## JWT
https://dev.to/cuongld2/create-apis-with-jwt-authorization-using-spring-boot-24f9  
https://medium.com/swlh/spring-boot-security-jwt-hello-world-example-b479e457664c    
https://github.com/jwtk/jjwt    
https://www.baeldung.com/java-storing-passwords    

## Tests
https://www.baeldung.com/spring-security-integration-tests  

## Security
### @PreAuthorize is executed after @Valid validation
https://github.com/spring-projects/spring-security/issues/6545  
https://stackoverflow.com/questions/22780668/how-to-check-security-acess-secured-or-preauthorize-before-validation-vali  

## JPA
https://www.baeldung.com/spring-data-jpa-pagination-sorting  
https://www.baeldung.com/spring-data-rest-customize-http-endpoints  
https://www.baeldung.com/spring-boot-data-sql-and-schema-sql  
https://medium.com/monstar-lab-bangladesh-engineering/jpa-hibernate-bidirectional-lazy-loading-done-right-65eda6426d64  

## Pagination
https://reflectoring.io/spring-boot-paging/  
https://github.com/thombergs/code-examples/blob/master/spring-boot/paging/src/test/java/io/reflectoring/paging/PagedControllerTest.java

## Swagger
https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Getting-started  
